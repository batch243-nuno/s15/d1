console.log("Hello World");

// [Section] Syntax, Statements and Comments
// Statements in programming are instructions that we tell the computer to perform.

// Variables are used to contain data, 
// Any information that is used by an application is stored in what we call the "memory". 
// when we create variabels, certain portions of a device's memory is given a name that we call variables.
// This makes it easier for us to associate information stored in our devices to actual "names" about information.
// Declaring variables 
// Declaring variables - it tells our devices that a variable name is created and is ready to store data. 
// Declaring a variable without giving it a value will automatically, assign it with the value of "undefined", meaning the variable's value was, not defined 
    // Syntax
        //let const variableName; 

let myVariable = "Ada Lovelace";
// console.log() is useful for printing values of variables or certain results of code into the Google Chrome browser's console.,
// Constant use of this throughtout developing an application will save us time and builds good habit in always checking for the output of our code.
console.log(myVariable);
/*
Guides in writing variables:
    1. use the let keyword followed the variable name of your choose and use the assignment operator (=) to assign value.
    2. Variable names should start with lowercase characeter, use camelCase for multiple words.
    3. For constant variables, use the 'const' keyword. using let: we can change the value of the variable.
    using const: we cannot change the value of the variable. 
    4. Variable names should be indicative (descritptive) of the value being stored to avoid confusion.
*/

// Declare and initialize varibles
//Initializing varibles - the instance when a variable is given it's inital/ starting value
    // Syntax
        // let/const variableName = value;

// let productName = "desktop computer";
// console.Log(productName)

let productPrice = 18999;
console.log(productPrice);

    // In the context of certain applications, some variables/ information are constant and should not be changed.
    // In this example, the interest rate for a loan, saving account or a mortgage must not be change due to real world concerns

const interest = 3.599;
console.log(interest);

// Reassigning variable values 1 Reassigning a varible means changing it's initial or previous value into another value. 
    // Syntax
        // variableName = newvalue;

productName = "laptop"; 
console.log(productName);
console.log(productName);

// let productName = "laptop"; 
// console.log(productName),
//let variable cannot be re-declared within its scope.
// Values of constants cannot be changed and will simpy return an error. 
// interest = 4.489; 
// console.log(interest);

// Reassigning variables and initializing variables

let supplier;

supplier ="Zuitt Store";
 // Reassignment supplier = "Zuitt merch";
const name = "George Alfred Cabaccang";
// name = "George Alfred Cabaccang";
// var vs let/const
// some of you may wonder why we used let and const keywrod in decalaring a variable when we search online, we usually see var. // var - is also used in declaring variable. but var is an EcmaScript1 feautre [ES1 (Javascript 1997)]|

// What makes let/const different var?
a = 5; console.log(a)
var a;
// b = 6; 
//console.log(b); 
// let b;

// let/const local/global scope
// Scope essentially means where these variables are available for use 
// let and const are block scoped.
// A block is a chunk of code bounded by {}. A block in curly braces. Anything within the curly braces is a block.

let outerVariable = "hello";
{
    let innerVariable = "fello";
    console.log(innerVariable);
    console.log(outerVariable);
}

console.log(outerVariable);

let productCode = "DC017", productBrand = "Dell";
console.log(productCode, productBrand);
console.log(productCode);
console.log(productBrand)

// Data types
// String - series of characters
// string can use either ('') and ("")

let country = "Philippines";
let province ='Batangas';
let fullAddress = province + ", " +country ;

console.log("Country is " + country +" and Province is " + province);
// console.log('Country is ${country} + and Province is ${province}');

let greeting = 'I live in the ' + fullAddress;
console.log(greeting);

// escape character
let mailAddress = "Metro Manila\nPhilippines";
console.log(mailAddress);

let message = "John's emplyotees went home early.";
message = 'John\'s emplyotees went home early.';

console.log(message);

// number
let count = "26";
let headCount = 26;
console.log(count);
console.log(headCount);

// decimal
let grade = 98.7;
console.log(grade);

// exponential
let planetDistance = 2e10;
console.log(planetDistance);

let merge = `grade is ${grade}`;
console.log(merge);

console.log("John's grade last quarter is " + grade);

// boolean
let isMarried = false;
let inGoodConduct = true;
console.log(isMarried);
console.log(inGoodConduct);
console.log(`is Married: ${isMarried}`);

// Arrays
    // similar data types
    let grades = [98.7, 92.1, 90.2, 94.6];
    console.log(grades);

    // different data types is allowed but not recommend
    let details = ["John", "Smith", 32, true];
    console.log(details);

// Objects
let person = {
    fullName: "Juan Dela Cruz",
    age: 35,
    isMarried:false,
    contact: [0912222222, 845677868],
    address: {
        houseNumber: '234',
        city:"Manila"
    }
};
console.log(person.contact[0]);

// typeof
console.log(typeof person.contact);
    /*
        Constant Objects and Arrays
            The keyword const is a little misleading.

            It does not define a constant value. It defines a constant reference to a value.

            Because of this you can NOT:

            Reassign a constant value
            Reassign a constant array
            Reassign a constant object

            But you CAN:

            Change the elements of constant array
            Change the properties of constant object

        */

const anime = ['one piece', 'one punch man', 'AOT'];
// anime = ['kimetsu no yaiba'];
console.log(anime);

anime[0] = 'kimetsu';
anime[3] = 'jojo'
console.log(anime);

// null - was created and assigned but not holding value
let spouse = null;
console.log(spouse);

// undefined - was created but no value
let unknown;
console.log(unknown);

// undifined vs null
// 